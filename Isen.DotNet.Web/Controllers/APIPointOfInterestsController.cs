﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Isen.DotNet.Library.Data;
using Isen.DotNet.Library.Models.Implementation;

namespace Isen.DotNet.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/poi")]
    public class APIPointOfInterestsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public APIPointOfInterestsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/APIPointOfInterests
        [HttpGet]
        public IEnumerable<PointOfInterest> GetPointOfInterestCollection()
        {
            return _context.PointOfInterestCollection
                .Include(poi => poi.Address)
                .Include(poi => poi.Address.Town)
                .Include(poi => poi.Address.Town.Departement)
                .Include(poi => poi.Category);
        }

        private bool PointOfInterestExists(int id)
        {
            return _context.PointOfInterestCollection.Any(e => e.Id == id);
        }
    }
}