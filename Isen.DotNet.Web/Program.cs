﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Isen.DotNet.Library.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Isen.DotNet.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            
            // Récupérer une instance de SeedData
            // en appelant le moteur d'injection de dépendances
            using (var scope = host.Services.CreateScope())
            {
                var hostingEnvironment = scope.ServiceProvider.GetService<IHostingEnvironment>();
                var seed = scope.ServiceProvider
                    .GetService<SeedData>();

                seed.DropDatabase();
                seed.CreateDatabase();

                seed.AddCities();
                seed.AddPersons();

                seed.AddCategories(Path.Combine(hostingEnvironment.WebRootPath,"Json","Categories.json"));
                seed.AddDepartements(Path.Combine(hostingEnvironment.WebRootPath, "Json", "Departements.json"));
                seed.AddTowns(Path.Combine(hostingEnvironment.WebRootPath, "Json", "Communes.json"));
                seed.AddAddressPoi(Path.Combine(hostingEnvironment.WebRootPath, "Json", "poi.json"));
            }
            
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
