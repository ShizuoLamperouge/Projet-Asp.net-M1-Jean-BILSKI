using Newtonsoft.Json;
using System;
using System.Dynamic;

namespace Isen.DotNet.Library.Models.Base
{
    public abstract class BaseModel
    {
        public int Id { get;set; }

        [JsonIgnore]
        public virtual string Display =>
            $"[Id={Id}]";

        public override string ToString() => Display;

        [JsonIgnore]
        public bool IsNew => Id <= 0;

        public virtual dynamic ToDynamic()
        {
            dynamic response = new ExpandoObject();
            response.id = Id;
            response.fetch = DateTime.Now;

            return response;
        }

    }
}