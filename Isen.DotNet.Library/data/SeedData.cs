using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Isen.DotNet.Library.Models.Implementation;
using Isen.DotNet.Library.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Isen.DotNet.Library.Data
{
    public class SeedData
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<SeedData> _logger;
        private readonly ICityRepository _cityRepository;
        private readonly IPersonRepository _personRepository;

        private readonly IAddressRepository _addressRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IDepartementRepository _departementRepository;
        private readonly IPointOfInterestRepository _pointOfInterestRepository;
        private readonly ITownRepository _townRepository;

        public SeedData(
            ApplicationDbContext context,
            ILogger<SeedData> logger,

            ICityRepository cityRepository,
            IPersonRepository personRepository,

            IAddressRepository addressRepository,
            ICategoryRepository categoryRepository,
            IDepartementRepository departementRepository,
            IPointOfInterestRepository pointOfInterestRepository,
            ITownRepository townRepository)
        {
            _context = context;
            _logger = logger;

            _addressRepository = addressRepository;
            _categoryRepository = categoryRepository;
            _departementRepository = departementRepository;
            _pointOfInterestRepository = pointOfInterestRepository;
            _townRepository = townRepository;

            _cityRepository = cityRepository;
            _personRepository = personRepository;
        }

        public void DropDatabase()
        {
            var deleted = _context.Database.EnsureDeleted();
            var not = deleted ? "" : "not ";
            _logger.LogWarning($"Database was {not}dropped.");
        }

        public void CreateDatabase()
        {
            var created = _context.Database.EnsureCreated();
            var not = created ? "" : "not ";
            _logger.LogWarning($"Database was {not}created.");
        }

        public void AddCities()
        {
            if (_cityRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding cities");

            var cities = new List<City>
            {
            };
            _cityRepository.UpdateRange(cities);
            _cityRepository.Save();

            _logger.LogWarning("Added cities");
        }

        public void AddPersons()
        {
            if (_personRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding persons");

            var persons = new List<Person>
            {
                new Person
                {
                    FirstName = "Calendau",
                    LastName = "GUQUET",
                    BirthDate = new DateTime(1980,2,28)
                },
                new Person
                {
                    FirstName = "John",
                    LastName = "APPLESEED",
                    BirthDate = new DateTime(1971,12,14)
                },
                new Person
                {
                    FirstName = "Steve",
                    LastName = "JOBS",
                    BirthDate = new DateTime(1949,2,24)
                }
            };
            _personRepository.UpdateRange(persons);
            _personRepository.Save();

            _logger.LogWarning("Added persons");
        }

        public void AddCategories(string path)
        {
            if (_categoryRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding categories");

            if (File.Exists(path))
            {
                var categoryCollection = new List<Category>();
                var json = File.ReadAllText(path);
                JArray jsonVal = JArray.Parse(json) as JArray;
                dynamic categories = jsonVal;
                foreach(var category in categories)
                {
                    categoryCollection.Add(
                            new Category()
                            {
                                Name = category.Nom,
                                Description = category.Description
                            }
                        );
                }
                _categoryRepository.UpdateRange(categoryCollection);
                _categoryRepository.Save();

                _logger.LogWarning("Added categories");
            }
            else
            {
                _logger.LogWarning("\n\ncategories.json not found : \n "+path+"\n\n");
            }

        }

        public void AddDepartements(string path)
        {
            if (_departementRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding departements");

            if (File.Exists(path))
            {
                var departementCollection = new List<Departement>();
                var json = File.ReadAllText(path);
                JArray jsonVal = JArray.Parse(json) as JArray;
                dynamic departements = jsonVal;
                foreach (var departement in departements)
                {
                    departementCollection.Add(
                            new Departement()
                            {
                                Name = departement.Nom,
                                Code = (string)departement.CodePostal
                            }
                        );
                }
                _departementRepository.UpdateRange(departementCollection);
                _departementRepository.Save();

                _logger.LogWarning("Added departement");
            }
            else
            {
                _logger.LogWarning("\n\ncategories.json not found : \n " + path + "\n\n");
            }
        }

        public void AddTowns(string path)
        {
            if (_townRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding towns");

            if (File.Exists(path))
            {
                var townCollection = new List<Town>();
                var json = File.ReadAllText(path);
                JArray jsonVal = JArray.Parse(json) as JArray;
                dynamic towns = jsonVal;
                foreach (var town in towns)
                {
                    if(
                        (town.PositionGPS != null) &&
                        (town.PositionGPS.Latitude != null) &&
                        (town.PositionGPS.Longitude != null)
                    )
                    {
                        townCollection.Add(
                                new Town()
                                {
                                    Name = town.Nom,
                                    Lattitude = (double)town.PositionGPS?.Latitude,
                                    Longitude = (double)town.PositionGPS?.Longitude,
                                    Departement = _departementRepository.GetAll().Where<Departement>((d) => d.Name == (string)town.Departement).FirstOrDefault()
                                }
                            );
                    }
                }

                _townRepository.UpdateRange(townCollection);
                _townRepository.Save();

                _logger.LogWarning("Added town");
            }
            else
            {
                _logger.LogWarning("\n\ncategories.json not found : \n " + path + "\n\n");
            }
        }

        public void AddAddressPoi(string path)
        {
            if (_addressRepository.GetAll().Any() && _pointOfInterestRepository.GetAll().Any()) return;
            _logger.LogWarning("Adding addresses & poi");

            if (File.Exists(path))
            {
                var addressCollection = new List<Address>();
                var poiCollection = new List<PointOfInterest>();
                var json = File.ReadAllText(path);
                JArray jsonVal = JArray.Parse(json) as JArray;
                dynamic values = jsonVal;
                foreach (var bundle in values)
                {
                    var address = new Address()
                    {
                        Text = (string)bundle.addresse.Text,
                        PostalCode = (int)bundle.addresse.PostalCode,
                        Lattitude = (double)bundle.addresse.Latitude,
                        Longitude = (double)bundle.addresse.Longitude,
                        Town = _townRepository.GetAll().Where((t) => t.Name == (string)bundle.addresse.Town).FirstOrDefault()
                    };

                    var poi = new PointOfInterest()
                    {
                        Name = (string)bundle.poi.Name,
                        Description = (string)bundle.poi.Description,
                        Category = _categoryRepository.GetAll().Where((c) => c.Name == (string)bundle.poi.Category).FirstOrDefault(),
                        Address = address
                    };
                    addressCollection.Add(address);
                    poiCollection.Add(poi);
                }
                _addressRepository.UpdateRange(addressCollection);
                _addressRepository.Save();
                _pointOfInterestRepository.UpdateRange(poiCollection);
                _pointOfInterestRepository.Save();

                _logger.LogWarning("Added addresses & poi");
            }
            else
            {
                _logger.LogWarning("\n\npoi.json not found : \n " + path + "\n\n");
            }
        }
    }
}