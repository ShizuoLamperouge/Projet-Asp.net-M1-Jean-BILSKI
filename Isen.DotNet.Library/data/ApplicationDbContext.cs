using Isen.DotNet.Library.Models.Implementation;
using Microsoft.EntityFrameworkCore;

namespace Isen.DotNet.Library.Data
{
    public class ApplicationDbContext : DbContext
    {
        // 1 - Préciser les DbSet
        public DbSet<City> CityCollection { get;set; }
        public DbSet<Person> PersonCollection { get;set; }

        public DbSet<Address> AddressCollection { get; set; }
        public DbSet<Category> CategoryCollection { get; set; }
        public DbSet<Departement> DepartementCollection { get; set; }
        public DbSet<PointOfInterest> PointOfInterestCollection { get; set; }
        public DbSet<Town> TownCollection { get; set; }
        
        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options) 
            : base(options) { }

        protected override void OnModelCreating(
            ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            // 2 - Configurer les mappings tables / classes
            builder.Entity<City>()
                .ToTable("City")
                .HasMany(c => c.PersonCollection)
                .WithOne(p => p.City)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Person>()
                .ToTable("Person")
                .HasOne(p => p.City)
                .WithMany(c => c.PersonCollection)
                .HasForeignKey(p => p.CityId);

            builder.Entity<Category>()
                .ToTable("Category");

            builder.Entity<Departement>()
                .ToTable("Departement");

            builder.Entity<Address>()
                .ToTable("Address");

            builder.Entity<PointOfInterest>()
                .ToTable("PointOfInterest")
                .HasOne(p => p.Address)
                .WithOne(a => a.PointOfInterest);

            builder.Entity<Town>()
                .ToTable("Town");

        }
    }
}