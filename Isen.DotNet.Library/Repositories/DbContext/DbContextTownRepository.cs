﻿using Isen.DotNet.Library.Data;
using Isen.DotNet.Library.Models;
using Isen.DotNet.Library.Models.Implementation;
using Isen.DotNet.Library.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Isen.DotNet.Library.Repositories.DbContext
{
    public class DbContextTownRepository :
        BaseDbContextRepository<Town>, ITownRepository
    {

        public DbContextTownRepository(
            ILogger<DbContextTownRepository> logger,
            ApplicationDbContext context)
            : base(logger, context)
        {
        }

        public override IQueryable<Town> Includes(
            IQueryable<Town> queryable)
                => queryable.Include(c => c.AddressCollection).Include(c => c.Departement);

    }
}
