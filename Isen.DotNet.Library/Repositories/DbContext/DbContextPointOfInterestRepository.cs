﻿using Isen.DotNet.Library.Data;
using Isen.DotNet.Library.Models.Implementation;
using Isen.DotNet.Library.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Isen.DotNet.Library.Repositories.DbContext
{
    public class DbContextDepartementRepository :
        BaseDbContextRepository<Departement>, IDepartementRepository
    {

        public DbContextDepartementRepository(
            ILogger<DbContextDepartementRepository> logger,
            ApplicationDbContext context)
            : base(logger, context)
        {
        }

        public override IQueryable<Departement> Includes(
            IQueryable<Departement> queryable)
                => queryable.Include(c => c.TownCollection);

    }
}
