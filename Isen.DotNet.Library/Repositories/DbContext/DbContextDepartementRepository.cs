﻿using Isen.DotNet.Library.Data;
using Isen.DotNet.Library.Models.Implementation;
using Isen.DotNet.Library.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Isen.DotNet.Library.Repositories.DbContext
{
    public class DbContextPointOfInterestRepository :
        BaseDbContextRepository<PointOfInterest>, IPointOfInterestRepository
    {

        public DbContextPointOfInterestRepository(
            ILogger<DbContextPointOfInterestRepository> logger,
            ApplicationDbContext context)
            : base(logger, context)
        {
        }

        public override IQueryable<PointOfInterest> Includes(
            IQueryable<PointOfInterest> queryable)
                => queryable.Include(c => c.Category).Include(c => c.Address);

    }
}
